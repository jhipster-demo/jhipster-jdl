# JDL samples

### Ecommerce Demo

To generate the code for ecommerce gateway and microservices, do the following:

1. In jhipster-invoice folder, run
   ```
   jhipster import-jdl ../jhipster-jdl/ecommerce-invoicems.jh
   ```
   
1. In jhipster-product folder, run
   ```
   jhipster import-jdl ../jhipster-jdl/ecommerce-productms.jh
   ```
   
1. In jhipster-notification folder, run
   ```
   jhipster import-jdl ../jhipster-jdl/ecommerce-notificationms.jh
   ```
   
1. In jhipster-store folder, run
   ```
   jhipster import-jdl ../jhipster-jdl/ecommerce-storegw.jh
   ```
   
   